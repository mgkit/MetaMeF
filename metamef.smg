/*

-----------------------------------------------

Copyright (C) 2018 Mathilde Regnault

-----------------------------------------------

metamef.smg : Metagrammar of medieval French

-----------------------------------------------

Description:
First part of a metagrammar for medieval French
based on FRMG by Eric de la Clergerie

-----------------------------------------------

*/

/* Pour les tests

diable categories
disable simple_categories
disable det
disable noun
disable cnoun
didsable pnoun
disable pronoun
disable prep
disable agreement
disable verb_agreement
disable verb_categorization
disable verb

*/

/* POS tags
det : déterminant
nc : nom commun
np : nom propre
prep : préposition
pro : pronom
verb

 */

/* Noeuds syntaxiques
N2 >> N
Nc
Np
det
SP >> Prep
S >> V

 */

/* Macros

 */


class categories {
  %% décrit une catégorie
  node Anchor : [type:anchor];
  desc.@htcat = node(Anchor).cat;
  node(Anchor).id = node(Anchor).cat;
  desc([ht:@ht_fs]);
}

class simple_categories {
  %% introduit un arbre ancré
  <: categories;
  desc([ht:@simple_ht]);
}


class det {
  %% catégorie grammaticale déterminant, est ancre lexicale, prend des arguments
  <: simple_categories;
  node det : [cat: det ]; det=Anchor;
  desc.ht = value([arg0: @emptyarg_fs,arg1: @emptyarg_fs,arg2: @emptyarg_fs]);
}


%% N : substantif, est une catégorie grammaticale, ancre lexicale, induit accords, peut être précédé d'un déterminant
class _noun {
  <: simple_categories;
  node N : [cat: N];
  - anchor::agreement; Anchor = anchor::N;
  - n::agreement; N = n::N;
  node(N).bot.headcat = node(Anchor).cat;
  %% Dominances : N2 domine N et det, et det précède N dans la phrase
  N2 >> N;
  N >> Anchor;
  node det : [cat: det, type: subst];
  N2 >> det;
  det < N; %%précédence non immédiate
  %% Dépendance entre det et N
  det =>
    node(N2).bot.sat = value(+); %% si det présent, alors nom saturé
  %% Eulalie : La domnizelle celle kose non contredist
  ~ det =>
    node(N2).bot.sat = value(-); %% sinon, nom non saturé
  %% Yvain : "Filz estes au roi Urien" mais sat autrement
  %% si besoin, ajouter : node(N2).bot.countable = value(-),
  %%node(N2).bot.wh = value(-);
  %% si besoin : node(det).top.countable = node(N2).bot.countable; %% det type numéro, propriétés viennent du N ou du det
}


class noun {
  <: _noun
  node(Anchor).bot.person = value(3); %% Si le sujet est un SN, alors c'est une 3e pers.
  %%ajouter une co-ancre pour le titre (sire, messire, sieur, dame...)
}


class cnoun {
  %% cnoun : nom commun
  <: noun;
  N >> Nc; %% noeud N domine
  Nc=Anchor; %% ancre lexicale
  node Nc : [cat: nc]; %% categorie grammaticale
}


class pnoun {
  %% noms propres
  <: noun;
  N2 >> N; N >> Np; %% dominance
  Np=Anchor; %% ancre lexicale
  node Np : [cat: np]; % categorie grammaticale
  node(Np).bot.sat = value(+); %% par nature réfère à une entité unique donc pas besoin de détermination
}

class pronoun {
  %% pronoms
  <: _noun;
  node(Anchor).cat = value(pro);
}


class prep {
  %% prépositions : classe à étendre (uniquement prep + SN pour l'instant)
  <: simple_categories;
  desc.@real0 = node(Prep).top.real; %%deFRMG
  node Prep : [cat: prep]; %% categorie grammaticale
  Prep=Anchor; %% ancre lexicale
  %% introduit un syntagme nominal
  Prep < N2 =>
    Prep >> N2,
    SP >> Prep;
    }




class agreement {
  %% Generic class to add agreement equations
  + agreement;
  father(N).bot.number = node(N).top.number;
  father(N).bot.gender = node(N).top.gender;
  father(N).bot.person = node(N).top.person;
  %% indice mineur et peu fiable : father(N).bot.case = node(N).top.case;
}


class verb_agreement {
  %% accord verbal
  + vagreement;
  father(V).bot.number = node(V).top.number;
  father(V).bot.gender = node(V).top.gender;
  father(V).bot.tense = node(V).top.tense;
  father(V).bot.mode = node(V).top.mode;
  father(V).bot.person = node(V).top.person;
}


%% Verbal categorization (valence)
class verb_categorization {
  + verbalCategorization;
  %%node(v).cat = value(v|aux);
  %%pour l'instant, seulement v
  node(v).cat = value(v);

  %%arg0 = sujet ?
  - arg0::varg; desc.@arg0 = $arg0::arg;
  S=arg0::S; v=arg0::v; V=arg0::V; Infl=arg0::Infl; V1 = arg0::V1; VMod = arg0::VMod;
  %%Ne pas préciser l'ordre (dans l'idéal) postsubj = arg0::postsubj; VSubj = arg0::VSubj;
  $arg0::arg.kind = value(-|subj|nosubj|prepobj);

	 %%arg1 = complément premier ?
	 - arg1::varg; desc.@arg1 = $arg1::arg;
	 S=arg1::S; v=arg1::v; V=arg1::V;Infl=arg1::Infl; V1 = arg1::V1; VMod = arg1::VMod;
	 %%postsubj = arg1::postsubj; VSubj = arg1::VSubj;
	 %%VModNcPred = arg1::VModNcPred;

	 %%arg2 = complément second ?
	 - arg2::varg; desc.@arg2 = $arg2::arg;
	 S=arg2::S; v=arg2::v; V=arg2::V;Infl=arg2::Infl; V1 = arg2::V1; VMod = arg2::VMod;
	 %%postsubj = arg2::postsubj;
	 %%VModNcPred = arg2::VModNcPred;
	 %% subarg info not yet used for arg0 and arg2
	 desc.@arg0.subarg = value(-); %% ?
	 desc.@arg2.subarg = value(-); %% ?
	 }


%% V : est tête de phrase, marqué par l'accord avec le sujet
   class verb {
   %%<: _verb;
   node S:[id:S,cat:S];
   S >> V;
   - V::vagreement; V=V::V;
   - verbalCategorization;
  }
